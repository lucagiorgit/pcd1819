package pcd.lab04.mandel_wrong;

import javax.swing.*;
import java.awt.*;
import java.awt.image.*;

public class MandelbrotPanel extends JPanel {

	/*
	 * Matrice di pixel in cui si può specificare: colore, trasparenza, ...
	 */
	private BufferedImage image;
	
	public MandelbrotPanel(int w, int h){
		this.image = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
	}

	public void updateImage(int[] rgbData){
		int w = image.getWidth();
		int h = image.getHeight();
        image.setRGB(0, 0, w, h, rgbData, 0, w);
        repaint();
	}

	/**
	 * Override che viene invocato da edt
	 * @param g
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);   
        Graphics2D g2 = (Graphics2D)g; // è lui che indica come disegnare l'oggetto.
        g2.drawImage(image, 0, 0, null);  
   }
}
