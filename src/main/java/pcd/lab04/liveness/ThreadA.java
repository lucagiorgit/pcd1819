package pcd.lab04.liveness;

public class ThreadA extends BaseAgent {
 
	private LeftRightDeadlock res;
	
	public ThreadA(LeftRightDeadlock res){
		this.res = res;
	}
	
	public void run(){
		while (true){
			waitAbit();
			res.rightLeft();
		}
	}	
}
