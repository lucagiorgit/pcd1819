package pcd.lab04.liveness;

public class ThreadB extends BaseAgent {
 
	private LeftRightDeadlock res;
	
	public ThreadB(LeftRightDeadlock res){
		this.res = res;
	}
	
	public void run(){
		while (true){
			waitAbit();
			res.leftRight();
		}
	}	
}
