package pcd.lab04.gui3_mvc_deadlock;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

class MyView extends JFrame implements ActionListener, ModelObserver {

	private MyController controller;
	private JTextField state;

	public MyView(MyController controller) {
		super("My View");

		this.controller = controller;

		setSize(400, 60);
		setResizable(false);

		JButton button1 = new JButton("Event #1");
		button1.addActionListener(this);

		JButton button2 = new JButton("Event #2");
		button2.addActionListener(this);

		state = new JTextField(10);

		JPanel panel = new JPanel();
		panel.add(button1);
		panel.add(button2);
		panel.add(state);

		setLayout(new BorderLayout());
	    add(panel,BorderLayout.NORTH);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}
		});
	}

	public void actionPerformed(ActionEvent ev) {
		try {
			controller.processEvent(ev.getActionCommand());
		} catch (Exception ex) {
		}
	}

	@Override
	public void modelUpdated(MyModel model) {
		try {
			System.out.println("[View] model updated => updating the view");
			// fa riferimento alla seconda regola delle GUI -> se ho un edt, solo lui può agire sulla GUI.
			// I Componenti swing non sono monitor, quindi x design, non possono andare a eseguire nulla.
			// Se non sono il dispatcher, delego a lui il compito ---> invoke()
			// la invoke è di 2 tipi: andWait o later().
			// il codice della classe view non è detto che sia eseguito da edt.
			// non posso invocare metodi sulla view -> violo il flusso di controllo.
			SwingUtilities.invokeAndWait(() -> {
				state.setText("state: " + model.getState());
				// model.update, essendo un monitor ha il lock, quando arrivo alla view, lei delega a edt, che però fa getState. -> Questo è già in lock
				// e di fatto non puuò eseguire -> va in deadlock.
			});

			/*
			// se fosse così, il deadlock si risolve, perchè la lock è rientrante.
			try {
                final iny state = model.getState();
                    SwingUtilities.invokeAndWait(() -> {        // anche solo con invokeLater() sarei a posto.. lascio libera la lock
                    state.setText("state: " + s);
			    });
             */
        } catch (Exception ex){
			ex.printStackTrace();
		}
	}
}
