package pcd.lab06.executors.quad2_withsynch;

public class QuadratureResult {
	
	private double sum; 
	private int nTotalResultsToWait;
	private int nResultsArrived;
	
	public QuadratureResult(int nResults){
		nTotalResultsToWait = nResults;
		nResultsArrived = 0;
	}
	
	public synchronized void add(double value){
		sum += value;
		nResultsArrived++;
		if (nResultsArrived >= nTotalResultsToWait){
			notifyAll();
		}
	}

	public synchronized double getResult() throws InterruptedException {
		while (nResultsArrived < nTotalResultsToWait){ // tutte le volte che lo chiamo verifica che tutti
														// i risultati siano arrivati. altrimenti va in wait
														// e viene risvegliato solo se la add mi fa notify
			wait();
		}
		return sum;
	}
}
