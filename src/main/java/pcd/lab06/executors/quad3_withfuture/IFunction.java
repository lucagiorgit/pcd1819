package pcd.lab06.executors.quad3_withfuture;

public interface IFunction {

	public double eval(double val);
	
}
