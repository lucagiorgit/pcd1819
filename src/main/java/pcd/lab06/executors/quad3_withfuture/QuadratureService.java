package pcd.lab06.executors.quad3_withfuture;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class QuadratureService  {

	private int numTasks;
	private ExecutorService executor;
	private List<Future<Double>> resultSet = new ArrayList<>();
	
	public QuadratureService (int numTasks, int poolSize){		
		this.numTasks = numTasks;
		executor = Executors.newFixedThreadPool(poolSize);
	}
	
	public double compute(IFunction mf, double a, double b) throws InterruptedException {

		double x0 = a;
		double step = (b-a)/numTasks;		
		for (int i=0; i<numTasks; i++) {
			try {
				Future fut = executor.submit(new ComputeAreaTask(x0, x0 + step, mf));
				resultSet.add(fut);
				log("submitted task "+x0+" "+(x0+step));
				x0 += step;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		double res = 0;
		for(Future<Double> fut : resultSet){
			try {
				//print the return value of Future, notice the output delay in console
				// because Future.get() waits for task to get completed
				res += fut.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		return res;
	}
	
	
	private void log(String msg){
		System.out.println("[SERVICE] "+msg);
	}
}
