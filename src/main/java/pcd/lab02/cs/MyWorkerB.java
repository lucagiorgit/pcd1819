package pcd.lab02.cs;

public class MyWorkerB extends Worker {
	
	private Object lock;
	
	public MyWorkerB(String name, Object lock){
		super(name);
		this.lock = lock;
	}

	public void run(){
		while (true){
		  synchronized(lock){
			  action1();	
			  action2();

			  double loop = Math.random();
			  if(loop < 0.2){
				System.out.println("!!! Deadlock");
				while (true){}
			  }
		  }
		  action3();
		}
	}
	
	protected void action1(){
		println("b1");
		wasteRandomTime(0,100);
	}
	
	protected void action2(){
		println("b2");
		wasteRandomTime(10,20);
	}
	protected void action3(){
		println("b3");
		wasteRandomTime(100,200);
	}
}
