package pcd.lab07.vertx;

import io.vertx.core.*;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;

public class Test5_composeq {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		FileSystem fs = vertx.fileSystem();   
		
		Future<Void> failFuture = Future.future();

		Future<Buffer> f1 = Future.future();
		fs.readFile("build.gradle", f1);
		
		f1.compose(res -> {
			log("BUILD => \n"+res.toString().substring(0,160));
			Future<Buffer> f2 = Future.future();
			fs.readFile("settings.gradle", f2);
			/*
			int a = 1;
			a++;
			if (a == 2) throw new RuntimeException();
			*/
			return f2;
		}).compose(res -> {
			log("SETTINGS => \n"+res.toString().substring(0,160));
		}, failFuture);
		
		failFuture.setHandler(res -> {
			log("Something failed.");
		});
	}

	private static void log(String msg) {
		System.out.println("" + Thread.currentThread() + " " + msg);
	}
}

