package pcd.lab05.monitors.resman;

import java.util.ArrayList;
import java.util.List;

public class ResManagerImpl implements ResManager {
	private int nResourcesAvailable;
	private List<Boolean> resourcesArray;

	public ResManagerImpl(int nResourcesAvailable) {
		this.nResourcesAvailable = nResourcesAvailable;
		resourcesArray = new ArrayList<>();
		for (int i = 0; i < nResourcesAvailable; i++){
			resourcesArray.add(true);
		}
	}
	
	@Override
	public synchronized int get() throws InterruptedException {
		while (resourcesArray.isEmpty() || allResourcesTaken()){
			wait();
		}
		System.out.println("Wait -> " + resourcesArray.toString());
		return getFreeResourceIndex();
	}

	@Override
	public synchronized void release(int id) {
		this.resourcesArray.set(id, true);

		System.out.println("Release -> " + resourcesArray.toString());
		notifyAll();
	}

	private boolean allResourcesTaken(){
		boolean allTaken = true;
		for(int i = 0; i < nResourcesAvailable; i++){
			if (resourcesArray.get(i)){
				allTaken = false;
			}
		}
		return allTaken;
	}

	private int getFreeResourceIndex(){
		for(int i = 0; i < nResourcesAvailable; i++){
			if (resourcesArray.get(i)){
				resourcesArray.set(i, false);
				return i;
			}
		}
		return -1; // non deve succedere.
	}
}
