package pcd.lab05.monitors.barrier.sol;

import pcd.lab05.monitors.cyclic_barrier.Barrier;

/*
 * Barrier - to be implemented
 */
public class FakeBarrier implements Barrier {

	private int nParticipants;
	
	public FakeBarrier(int nParticipants) {
		this.nParticipants = nParticipants;
	}
	
	@Override
	public void hitAndWaitAll() throws InterruptedException {
		
	}

	
}
