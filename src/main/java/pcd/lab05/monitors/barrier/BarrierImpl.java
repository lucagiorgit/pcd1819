package pcd.lab05.monitors.barrier;

/*
 * Barrier - to be implemented
 */
public class BarrierImpl implements Barrier {

	private int nParticipants;
	private int hitCounter = 0;
	
	public BarrierImpl(int nParticipants) {
		this.nParticipants = nParticipants;
	}
	
	@Override
	public synchronized void hitAndWaitAll() throws InterruptedException {
		hitCounter++;
		while (hitCounter < nParticipants){
			wait();
		}
		notifyAll();
	}

	
}
