package pcd.lab05.monitors.cyclic_barrier.sol;

import java.util.Random;

public class Worker extends Thread {

	private Barrier barrier;
	private int counter;
	
	public Worker(String name, Barrier barrier) {
		super(name);
		this.barrier = barrier;
		counter = 0;
	}
	
	public void run() {
		Random gen = new Random(System.nanoTime());
		try {
			while (true) {
				log("stage: "+counter);
				waitFor(gen.nextInt(1000));
				barrier.hitAndWaitAll();
				counter++;
			}
		} catch (InterruptedException ex) {
			log("Interrupted!");
		}
	}
	
	private void log(String msg) {
		synchronized(System.out) {
			System.out.println("[ "+getName()+" ] "+msg);
		}
	}
	
	private void waitFor(long ms) throws InterruptedException{
		Thread.sleep(ms);
	}
}
