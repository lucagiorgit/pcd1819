package pcd.lab08.rx;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.reactivex.*;
import io.reactivex.schedulers.*;

public class Test04b_schedulers {

	public static void main(String[] args) throws Exception {

		// happens in IO sched
		Observable.just("a/1/t","6/b","1/2/f")
			.subscribeOn(Schedulers.io())
			.flatMap(s -> {
				System.out.println("flatMap on "+Thread.currentThread().getName());
				return Observable.fromArray(s.split("/"));
			})
		// happens on computation
			.observeOn(Schedulers.computation())
			.filter(s -> s.matches("[0-9]+"))
			.map(Integer::valueOf)
			.reduce((total,next) -> total + next)
			.subscribe(i -> System.out.println("> "+i+" on "+Thread.currentThread().getName()));

		Thread.sleep(1000);

	}

}
