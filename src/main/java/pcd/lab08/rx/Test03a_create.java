package pcd.lab08.rx;

import io.reactivex.*;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.subjects.AsyncSubject;

public class Test03a_create {

	public static void main(String[] args){
		
	    System.out.println("Creating a (cold) observable.");

	    Observable<Integer> source = Observable.create(emitter -> {
	        for (int i = 0; i <= 2; i++) {
	            log("source: " + i);
	            emitter.onNext(i);
	        }
	        emitter.onComplete();
	    });

	    source.subscribe(v -> log("A: "+v));
	    source.subscribe(v -> log("B: "+v));

	    //
	    /*
	    Observable.create(emitter -> {
	        while (!emitter.isDisposed()) {
	            long time = System.currentTimeMillis();
	            emitter.onNext(time);
	            if (time % 2 != 0) {
	                emitter.onError(new IllegalStateException("Odd millisecond!"));
	                break;
	            }
	        }
	   })
	   .subscribe(System.out::println, Throwable::printStackTrace);
	   */
	}
	
	static private void log(String msg) {
		System.out.println("[" + Thread.currentThread() + "] " + msg);
	}
}
